- **TODO**: make `about` accessible for unlogged users
- **TODO**: add ribbon with version and contacts + `about` link
- **TODO**: remove navbar for unlogged users
- **TODO**: manage profile deletion-update
- **TODO**: add to waiting list loading
- **TODO**: fix audio visu


- **DONE** set a basic theme with boostrap (used css vars)
- **DONE** setup dark theme