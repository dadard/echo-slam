# echo-slam

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Resources
- `src/assets/wallpapers/default/es_wallpaper_steam.jpg`: [Quake and fissure](https://dota2.gamepedia.com/Quake_and_Fissure)
- `src/assets/wallpapers/default/reddit_slam_blurred.jpg`: [@Nhim_Art](https://twitter.com/Nhim_Art)
- `src/assets/wallpapers/planetfall/es_wallpaper_planetfall.jpg`: [Planetfall preview](https://dota2.gamepedia.com/File:Planetfall_Preview_10.jpg)
- `src/assets/wallpapers/default/hud_empowered.png`: [Empowered_HUD](https://dota2.gamepedia.com/File:Empowered_16x9.png)
- `music_visualizer`: [Nick Jones](https://codepen.io/nfj525) from [codepen.io](https://codepen.io)


