import Vue from 'vue'
import App from './App.vue'

import "@/assets/css/main.css"
import "@/assets/css/totem-button.css"
import "@/assets/css/totem-animation.css"
import "@/assets/css/transition.css"
import VueRouter from "vue-router";
import HomeView from "@/components/connected/HomeView";
import ProfileView from "@/components/connected/ProfileView";
import LibraryView from "@/components/connected/LibraryView";
import DashboardView from "@/components/connected/DashboardView";
import MusicsView from "@/components/connected/MusicsView";
import AboutView from "@/components/notConnected/AboutView";
import Home from "@/components/connected/Home";
import ConnectionForm from "@/components/notConnected/ConnectionForm";
import NotFound from "@/components/notConnected/NotFound";

Vue.config.productionTip = false;

Vue.use(VueRouter);

const routes = [
  {path: "", component: Home,
  children: [
    {path: "", component: HomeView},
    {path: "/profile", component: ProfileView},
    {path: "/library", component: LibraryView},
    {path: "/musics", component: MusicsView},
    {path: "/dashboard", component: DashboardView},
  ]},
  {path: "/about", component: AboutView},
  {path: "/connect", component: ConnectionForm},
  {path: "/404", component: NotFound},
  {path: "*", redirect: "/404"}
];

const router = new VueRouter({
  mode: "history",
  routes: routes
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
