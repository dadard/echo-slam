const axios = require('axios');
const config = require('../config/config.json');

function handleError(error) {
    if (error.response) {
        throw error.response.data.Message
    } else {
        throw error
    }
}

export default class miscService {
    Message;
    Content;
    Status;

    constructor() {
        this.service = axios.default.create({
            baseURL: config.apiHost
        });
        this.routes = {
            profileUsernameCheck: "/profile/public",
            avatarList: "/avatar/list",
            apiInfos: "/infos",
            healthApis: "/health/apis",
            uploads: "/musics/lastadded",
            lastConnectedUsers: "/profile/public/last"
        }
    }

    checkProfileUsername(username) {
        return this.service.post(this.routes.profileUsernameCheck, {}, {
            params: {
                username: username
            }
        })
            .then(function(res) {
                return res.data.Content;
            })
            .catch(function(err) {
                handleError(err);
            })
    }

    getAvatarList() {
        return this.service.get(this.routes.avatarList)
            .then(function(res) {
               return res.data.Content;
            })
            .catch(function(err) {
                handleError(err)
            });
    }

    getAvatarUrl(name) {
        return config.apiHost + "/avatar?name=" + name
    }

    getApiInfos() {
        return this.service.get(this.routes.apiInfos)
            .then(function(res) {
                return res.data;
            })
            .catch(function(err) {
                handleError(err);
            })
    }

    getHealth() {
        return this.service.get(this.routes.healthApis)
            .then(function(res) {
                return res.data;
            })
            .catch(function(err) {
                handleError(err);
            })
    }

    getLastAddedList(jwt) {
        return this.service.get(this.routes.uploads, {
            headers: {
                Authorization: jwt
            }
        })
            .then(function(res) {
                return res.data.Content;
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    getLastConnectedUsers(jwt) {
        return this.service.get(this.routes.lastConnectedUsers, {
            headers: {
                Authorization: jwt
            }
        })
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }
}

export class Result {
    Title;
    Album;
    Artist;
    PublishedAt;
    ImageUrl;
    Genres;

    Available;

    AddedAt;
    AddedBy;
}

