const axios = require('axios');
const config = require('../config/config');

function handleError(error) {
    if (error.response) {
        throw error.response.data.Message
    } else {
        throw error
    }
}

export default class profileService {
    Message;
    Content;
    Status;

    constructor() {
        this.service = axios.default.create({
            baseURL: config.apiHost
        });
        this.routes = {
            profile: "/profile",
            signIn: "/signin",
            signUp: "/signup",
            player: "/profile/player"
        }
    }

    signUp  (username, password, recoverBy, contact, avatar) {
        return this.service.post(this.routes.signUp, {}, {
            params: {
                recover_by: recoverBy,
                contact: contact,
                avatar: avatar,
            },
            auth: {
                username: username,
                password: password
            }
        })
            .then(function(response) {
                return response.data.Message
            })
            .catch(function(error) {
                handleError(error)
            })
    }

    signUpConfirm (username, password, code) {
        return this.service.post(this.routes.profile, {}, {
            params: {
                code: code
            },
            auth: {
                username: username,
                password: password
            }
        })
            .then(function(response) {
                return response.data.Message;
            })
            .catch(function(error) {
                handleError(error)
            })
    }

    signIn (username, password) {
        return this.service.get(this.routes.signIn, {
            auth: {
                username: username,
                password: password
            }
        })
            .then(function(response) {
                return response.data
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    checkJwt(jwt) {
        return this.service.post(this.routes.signIn, {}, {
            headers: {
                Authorization: jwt
            }
        })
            .then(function(response) {
                return response.data.Status
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    getProfile(jwt) {
        return this.service.get(this.routes.profile, {
            headers: {
                Authorization: jwt
            }
        })
            .then(function(response) {
                return response.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    updatePlayer(jwt, title, artist, album, url) {
        return this.service.put(this.routes.player, {}, {
            params: {
                title: title,
                artist: artist,
                album: album,
                url: url
            },
            headers: {
                Authorization: jwt
            },
        })
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }
}