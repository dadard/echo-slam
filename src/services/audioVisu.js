export default class audioVisu {
    constructor(audioPlayerId, canvasId) {
        this.audioPlayer = document.getElementById(audioPlayerId);

        this.canvas = document.getElementById(canvasId);

        this.canvas.width = 664;
        this.canvas.height = 100;

        this.ctx = this.canvas.getContext("2d");

        let context = new AudioContext();
        let src = context.createMediaElementSource(this.audioPlayer);
        let analyser = context.createAnalyser();

        src.connect(analyser);
        analyser.connect(context.destination);
        analyser.fftSize = 256;
        let bufferLength = analyser.frequencyBinCount;


        let dataArray = new Uint8Array(bufferLength);

        let WIDTH = this.canvas.width;
        let HEIGHT = this.canvas.height;

        let barWidth = (WIDTH / bufferLength) * 2.5;
        let barHeight;
        let x = 0;

        let self = this;

        function renderFrame() {
            requestAnimationFrame(renderFrame);

            x = 0;

            analyser.getByteFrequencyData(dataArray);

            self.ctx.fillStyle = "black";
            self.ctx.fillRect(0, 0, WIDTH, HEIGHT);

            for (let i = 0; i < bufferLength; i++) {
                barHeight = dataArray[i] * 0.25;

                let r = barHeight + (25 * (i/bufferLength));
                let g = 250 * (i/bufferLength);
                let b = 50;

                self.ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
                self.ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

                x += barWidth + 1;
            }
        }

        renderFrame();
    }

    load(url) {
        this.audioPlayer.src = url;
        this.audioPlayer.load();
    }

    play() {

        if (!this.isPlaying()) {
            this.audioPlayer.play();
        }
    }

    pause() {
        if (this.isPlaying()) {
            this.audioPlayer.pause();
        }
    }

    mute() {
        this.audioPlayer.muted = true;
    }

    unmute() {
        this.audioPlayer.muted = false;
    }

    isPlaying() {
        return !this.audioPlayer.paused;
    }

    setVolume(v) {
        this.audioPlayer.volume = v;
    }

    setCurrentTime(tPercent) {
        this.audioPlayer.currentTime = this.audioPlayer.duration * tPercent / 100;
    }

}