const axios = require('axios').default;
const config = require('../config/config.json');

function handleError(error) {
    if (error.response) {
        throw error.response.data.Message
    } else {
        throw error
    }
}

export default class libraryService {
    constructor(jwt) {
        this.service = axios.create({
            baseURL: config.apiHost,
            headers: {
                Authorization: jwt
            }
        });

        this.routes = {
            library: "/library",
            libraryArtists: "/library/artist/list",
            libraryAlbums: "/library/album/list",

            availableArtists: "/musics/artist/list",
            availableAlbums: "/musics/album/list",
        }
    }

    getArtistList() {
        return this.service.get(this.routes.libraryArtists)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(error) {
                handleError(error)
            })
    }

    getAlbumList() {
        return this.service.get(this.routes.libraryAlbums)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(error) {
                handleError(error)
            })
    }

    getAvailableArtistList() {
        return this.service.get(this.routes.availableArtists)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    getAvailableAlbumList() {
        return this.service.get(this.routes.availableAlbums)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    addToLibrary(title, artist) {
        return this.service.post(this.routes.library, {}, {
            params: {
                title: title,
                artist: artist,
            }
        })
            .then(function(res) {
                return res.data.Message
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    removeFromLibrary(title, artist) {
        return this.service.delete(this.routes.library, {
            params: {
                title: title,
                artist: artist,
            }
        })
            .then(function(res) {
                return res.data.Message
            })
            .catch(function(e) {
                handleError(e)
            })
    }
}