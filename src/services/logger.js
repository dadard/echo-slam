export default class loggerService {
    constructor() {
        this.messages = [];
    }

    log(msg) {
        console.log(msg);
        this.messages.push(null);

        let self = this;

        setTimeout(function() {
            const fs = msg.toString();

            const f = fs[0].toUpperCase() + fs.slice(1);
            self.messages.push(f);

            setTimeout(function() {
                self.messages.push(null);
            }, 3000)
        }, 200);
    }

    getLastMessage() {
        return this.messages[this.messages.length - 1]
    }

    notEmpty() {
        return this.messages.length > 0
            && this.messages[this.messages.length - 1] != null;
    }
}