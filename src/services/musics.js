const config = require('../config/config.json');
const axios = require('axios');

function handleError(error) {
    if (error.response) {
        throw error.response.data.Message
    } else {
        throw error
    }
}

export default class musicsService {
    constructor(jwt) {
        this.service = axios.default.create({
            baseURL: config.apiHost,
            headers: {
                Authorization: jwt
            }
        });
        this.routes = {
            search: "/musics/search",
            waitingList: "/waiting-list",
            job: "/job",
            jobDone: "/job/done",
            musics: "/musics",
        }
    }

    getMusic(title, artist) {
        return this.service.get(this.routes.musics, {
            params: {
                title: title,
                artist: artist,
            }
        })
            .then(function(res) {
                return res.data.Content;
            })
            .catch(function(e) {
                handleError(e)
            })
    }

    search(query) {
        return this.service.get(this.routes.search, {
            params: {
                q: query
            }
        })
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    addToWaitingList(title, artist, album, publishedAt, imageUrl, genre) {
        let body = {
            "Title": title,
            "Artist": artist,
            "Album": album,
            "PublishedAt": publishedAt,
            "ImageUrl": imageUrl,
            "Genres": genre
        };

        return this.service.post(this.routes.waitingList, body)
            .then(function(res) {
                return res.data.Message
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    removeFromWaitingList(title, artist, album) {
        return this.service.delete(this.routes.waitingList, {
            params: {
                title: title,
                artist: artist,
                album: album
            }
        })
    }

    getWaitingList() {
        return this.service.get(this.routes.waitingList)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    createJob() {
        return this.service.post(this.routes.job)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    getJobStatus() {
        return this.service.get(this.routes.job)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }

    getLastJobStatus() {
        return this.service.get(this.routes.jobDone)
            .then(function(res) {
                return res.data.Content
            })
            .catch(function(err) {
                handleError(err)
            })
    }
}